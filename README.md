# Snapdragon Neural Processing Engine (SNPE)

## SNPE 1.53 (Sep. 2021)
### Installation with docker
1. (local)先把snpe1.53....zip下載後解壓縮
2. (local)`bin/dependencies.sh`最底下`sudo apt-get install $j` 加上`-y`
3. (local)`bin/check_python_depends.sh` 加上`pip3 install ${needed_depends_pip[i]`
    <img src="add_line1.png" alt="drawing" width="1200">
4. (local)開container (需用ubuntu18.04),並掛載dev使container可以抓到device
    ```
    docker run -it --name snpe \
        -v ${PWD}:/snpe \
        --privileged -v /dev/bus/usb:/dev/bus/usb \ 
        ubuntu:18.04
    ```
    * 註: 若container內adb devices抓不到，試著先關閉local pc上的adb
        ```
        adb kill-server
        ```
5. (container)安裝套件
    ```
    apt update
    apt install -y sudo
    apt install -y python3.6
    apt install -y python3-pip
    apt install -y adb 
    pip3 install tensorflow
    echo "alias python=python3.6" >> ~/.bashrc
    echo "export SNPE_ROOT=/snpe" >> ~/.bashrc
    source ~/.bashrc
    echo "export PYTHONPATH=$SNPE_ROOT/lib/python" >> ~/.bashrc
    source ~/.bashrc
    ```
6. (container)設定snpe環境
    ```
    sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 10
    source bin/dependencies.sh 
    source bin/check_python_depends.sh 
    source bin/envsetup.sh -t <TF_PATH>
    ```
7. (container)準備測試model (InceptionV3 in Tensorflow)
    ```
    python models/inception_v3/scripts/setup_inceptionv3.py -a models/inception_v3/ -d -r <Target Device>

    -r is optional, CPU is the target device if left blank
    <Target Device> can be aip / dsp
    ```
    After this step you will see folders raw data and dlc generated inside `models/inception_v3`
8. (container)dlc-viewer使用
   ```
    python bin/x86_64-linux-clang/snpe-dlc-viewer \
    -i models/inception_v3/dlc/inception_v3_quantized.dlc \
    -s models/inception_v3/dlc/inception_v3_quantized.html
   ```
   此步驟會在dlc/底下產出inception_v3_quantized.html，用browser即可打開
9. Benchmark
    * 照著benchmarks/alexnet_sample.json複製一份你的測試
        * Runtimes設定AIP
        * ProfilingLevel設定detailed
        * Devices設定adb devices
        * 剩下自己看
    * 跑benchmark
        ```
        python benchmarks/snpe_bench.py -c benchmarks/model.json
        ```
    * 在指定路徑找到results

### From DockerHub
1. Pull from DockerHub
   ```
   docker pull nder100kg/snpe:1.53
   ```
2. 照著[Installation with docker](#installation-with-docker) 4. 7. 8. 9.

### SNPE introduction
1. There are three parts of units with hexagon: Q6, HVX and HTA. However, there are still no framwork which provides access to HTA. So the only way we can use HTA for computation is using [SNPE sdk](https://developer.qualcomm.com/docs/snpe/overview.html) provided by Qualcomm

    <img src="ARM_arch.png" alt="drawing" width="400">
    <img src="aip_runtime.png" alt="drawing" height="400">

2. SNPE provides compiler to convert Tensorflow(.pb), Onnx(.onnx), Caffe2(.caffemodel) to SNPE DLC file
    * [tensorflow](https://developer.qualcomm.com/docs/snpe/model_conv_tensorflow.html): 
        ```
        snpe-tensorflow-to-dlc  --input_network model.pb \
                                --input_dim input "1,299,299,3" \ 
                                --out_node output \ 
                                --output_path model.dlc 
        ```
    * This step will make decision of how the model looks like, we can cut branch or some of nodes in this step

3. Quantize DLC: in this step, quantizer will sum up the min/max using files in image_file_list.txt
   * Command: 
        ```
        snpe-dlc-quantize   --input_dlc model.dlc 
                            --input_list image_file_list.txt
                            --output_dlc model_quantized.dlc
        ```
    * Qualcomm suggested:
        ```
        For more robust quantization results, we recommend providing 50-100 examples of representative input data for the given model use case, without using data from the training set. 
        ```
    * image_file_list.txt example, the raw files can be generated using script in models/

4. Working onto arm64:
    * For general propose
    ```
    adb push $SNPE_ROOT/lib/aarch64-android-clang6.0/*.so /system/lib64
    ```
    * If DSP / AIP is needed
    ```
    adb push $SNPE_ROOT/lib/dsp/*.so /system/lib64
    adb shell
    export ADSP_LIBRARY_PATH="/system/li64;/system/lib/rfsa/adsp;/system/vendor/lib/rfsa/adsp;/dsp"
    export LD_LIBRARY_PATH=/vendor/lib64
    ```

5. Runtime test (ARM)
    * Command: 
    ```
    snpe-dlc-validator --runtime all / cpu / gpu / dsp / aip
    ```
    * If runtime available, terminal will show "Present"

6. DLC-viewer
    * Command:
    ```
    python snpe-dlc-viewer -i model.dlc
    ```

## SNPE 1.43 (Nov. 2020)
1. Inference Preformance
    * Model: 範例InceptionV3, 並去掉最後的Softmax(dlc-viewer 顯示其會在HVX上執行)
    * CORE: SnapDragon 855
    * SNPE(DSP): 13.53ms

        <img src="DSP.jpg" alt="drawing" width="400">

    * SNPE(AIP): 15.994ms

        <img src="AIP.jpg" alt="drawing" width="400">

    * TFLite(HVX): 8.401ms

        <img src="TFLITE.jpg" alt="drawing" width="400">

    * SNPE(DSP) + TFLITE: 14.27 + 12.98 ms

        <img src="DSP+TFLITE.jpg" alt="drawing" width="400">

    * SNPE(AIP) + TFLITE: 15.068 + 10.69 ms
        <img src="AIP+TFLITE.jpg" alt="drawing" width="400">

    * SNPE(DSP)與TFLITE明顯衝突，可見其都是在access HVX，估計hexagon controller會自行調整(不清楚是schedule或是給mem分配)

    * SNPE default 會call兩個中核來跑，目前沒辦法自行設定，估計是包在某個.so內
    * TFLITE跑得比SNPE快

9. 結論：目前SNPE可能還不適合實用，等未來更新
